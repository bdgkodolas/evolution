package hu.hb.evolution;

import java.awt.Dimension;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;

public class Main {
    public static final double STEP_INTERVAL = 0.01;
    
   public static void main(String[] args) {
       World w = new World();
       w.startGame();
       
       JFrame frame = new JFrame("Evolution");
       frame.pack();
       frame.setSize(new Dimension(600, 400));
       frame.setVisible(true);
       frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
       //frame.setResizable(false);
       
       Panel p = new Panel(w);
       frame.add(p);
       frame.addKeyListener(new PanelKeyListener(w));
       while(!false){
           w.step(STEP_INTERVAL);
           p.repaint();
           try {
               Thread.sleep(Math.round(STEP_INTERVAL*1000));
           } catch (InterruptedException ex) {
               Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
           }
       }
   }
}
