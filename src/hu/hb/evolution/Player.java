package hu.hb.evolution;

import java.awt.event.KeyEvent;

public class Player extends Creature{
    
    private boolean leftPressed, rightPressed;
    
    
    public void keyPressed(KeyEvent e) {
        if(e.getKeyCode() == KeyEvent.VK_LEFT) {
            leftPressed = true;
        } else if(e.getKeyCode() == KeyEvent.VK_RIGHT) {
            rightPressed = true;
        }
    }
    
    public void keyReleased(KeyEvent e) {
        if(e.getKeyCode() == KeyEvent.VK_LEFT) {
            leftPressed = false;
        } else if(e.getKeyCode() == KeyEvent.VK_RIGHT) {
            rightPressed = false;
        }
    }

    @Override
    public void changeDirection() {
        if(leftPressed)
            direction -= 0.03;
        else if(rightPressed)
            direction += 0.03;
    }
    
    
}
