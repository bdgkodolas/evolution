package hu.hb.evolution;

public class Bot extends Creature {
    
    private static final int MAX_LIMIT = 200;
    
    private int counter = 0, limit = MAX_LIMIT / 2;

    @Override
    public void changeDirection() {
        if(counter == limit) {
            direction = Math.random() * 2 * Math.PI;
            limit = (int)(Math.random() * MAX_LIMIT);
            counter = 0;
        } else {
            ++counter;
        }
    }
    
    
    
}
