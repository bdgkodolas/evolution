package hu.hb.evolution;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class PanelKeyListener implements KeyListener {
    
    private World world;

    public PanelKeyListener(World world) {
        this.world = world;
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        world.player.keyPressed(e);
    }

    @Override
    public void keyReleased(KeyEvent e) {
        world.player.keyReleased(e);
    }
    
}
